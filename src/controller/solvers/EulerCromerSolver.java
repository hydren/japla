package controller.solvers;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.physics.Body2D;

public class EulerCromerSolver extends Physics2DSolver
{
	public 
	EulerCromerSolver(Physics2D physics) 
	{
		super(physics);
		timestep = 0.1;
		displayName = "Semi-implicit Euler (Euler-Cromer)";
	}
	
	@Override
	public Physics2DSolver getNewInstance(Physics2D physics) {
		return new EulerCromerSolver(physics);
	}

	@Override public 
	void step() 
	{
		physics.computeAccelerations();
		
		//velocity & position loop
		for(Body2D b1 : physics.universe.bodies)
		{
			b1.velocity.add(b1.acceleration.times(timestep));
			b1.position.add(b1.velocity.times(timestep));
		}
		
		timeElapsed += timestep;
	}
}
