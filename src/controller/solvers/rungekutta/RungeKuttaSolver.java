package controller.solvers.rungekutta;

import java.lang.reflect.Array;
import java.util.HashMap;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.geometry.Vector2D;
import model.physics.Body2D;

public class RungeKuttaSolver extends Physics2DSolver
{
	final ButcherTable butcherTable;
	final int order;
	
	public RungeKuttaSolver(Physics2D physics, ButcherTable bt, String customName)
	{
		super(physics);
		this.timestep = 0.01;
		this.displayName = customName;
		this.butcherTable = bt;
		this.order = bt.size-1;
	}
	
	public RungeKuttaSolver(Physics2D physics, ButcherTable bt) 
	{
		this(physics, bt, "Runge-Kutta (order "+(bt.size-1)+")");
	}
	
	public RungeKuttaSolver(Physics2D physics) 
	{
		this(physics, ButcherTable.RK4_CLASSIC);
	}
	
	private double A(int i, int j) { return butcherTable.a(i, j); }
	private double B(int index) { return butcherTable.b(index); }
	private Vector2D get(HashMap<Body2D, Vector2D> map, Body2D body)
	{
		Vector2D v = map.get(body);
		if(v == null) return new Vector2D();
		else return v;
	}

	@Override
	public void step() 
	{
		@SuppressWarnings("unchecked")
		HashMap<Body2D, Vector2D> 
			wvs = new HashMap<Body2D, Vector2D>(), 
			wrs = new HashMap<Body2D, Vector2D>(), 
			kvs[] = (HashMap<Body2D, Vector2D>[]) Array.newInstance(HashMap.class, this.order+1), 
			krs[] = (HashMap<Body2D, Vector2D>[]) Array.newInstance(HashMap.class, this.order+1);
		
		for(int i = 1; i <= order; i++)
		{
			kvs[i] = new HashMap<Body2D, Vector2D>(); 
			krs[i] = new HashMap<Body2D, Vector2D>();
			
			for(Body2D body : physics.universe.bodies)
			{
				Vector2D skv = new Vector2D(), skr = new Vector2D();
				for(int j = 1; j < i; j++) //sum all previous k's (weighted according to butcher table)
				{
					if(A(i, j) == 0) continue; //don't sum if coefficient is zero (optimization)
					
					skv.add(get(kvs[j], body).times(A(i, j))); //velocity
					skr.add(get(krs[j], body).times(A(i, j))); //position
				}
				
				wvs.put(body, body.velocity.sum(skv.times(timestep))); //velocity
				wrs.put(body, body.position.add(skr.times(timestep))); //position
			}
			
			physics.derive(kvs[i], krs[i], wvs, wrs);
		}
		
		// compute next position/velocity
		for(int i = 1; i <= order; i++)
		{
			if(B(i) == 0) continue; //don't sum if coefficient is zero (optimization)
			
			for(Body2D body : physics.universe.bodies)
			{
				body.velocity.add(get(kvs[i], body).times(timestep * B(i))); //velocity
				body.position.add(get(krs[i], body).times(timestep * B(i))); //position
			}
		}
		
		timeElapsed += timestep;
	}

	@Override
	public Physics2DSolver getNewInstance(Physics2D physics) 
	{
		return new RungeKuttaSolver(physics, this.butcherTable, this.displayName);
	}

	public static class MidpointRK2Solver 	extends RungeKuttaSolver { public MidpointRK2Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK2_MIDPOINT, "Runge-Kutta (2nd order, midpoint)"); } }
	public static class HeunRK2Solver 		extends RungeKuttaSolver { public HeunRK2Solver(Physics2D physics) 		{ super(physics, ButcherTable.RK2_HEUN, "Runge-Kutta (2nd order, Heun)"); } }
	public static class RalstonRK2Solver 	extends RungeKuttaSolver { public RalstonRK2Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK2_RALSTON, "Runge-Kutta (2nd order, Ralston)"); } }
	public static class KuttaRK3Solver 		extends RungeKuttaSolver { public KuttaRK3Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK3_KUTTA, "Runge-Kutta (3rd order, Kutta)"); } }
	public static class HeunRK3Solver	 	extends RungeKuttaSolver { public HeunRK3Solver(Physics2D physics) 		{ super(physics, ButcherTable.RK3_HEUN, "Runge-Kutta (3rd order, Heun)"); } }
	public static class ClassicRK4Solver 	extends RungeKuttaSolver { public ClassicRK4Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK4_CLASSIC, "Runge-Kutta (4th order, classic)"); } }
	public static class Rule38RK4Solver 	extends RungeKuttaSolver { public Rule38RK4Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK4_RULE_3_8, "Runge-Kutta (4nd order, rule 3/8)"); } }
	public static class GillRK4Solver 		extends RungeKuttaSolver { public GillRK4Solver(Physics2D physics) 		{ super(physics, ButcherTable.RK4_GILL, "Runge-Kutta (4th order, Gill)"); } }
	public static class RalstonRK4Solver 	extends RungeKuttaSolver { public RalstonRK4Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK4_RALSTON, "Runge-Kutta (4th order, Ralston)"); } }
	public static class NystromRK5Solver 	extends RungeKuttaSolver { public NystromRK5Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK5_NYSTROM, "Runge-Kutta (5th order, Nystrom)"); } }
	public static class ButcherRK6Solver 	extends RungeKuttaSolver { public ButcherRK6Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK6_BUTCHER, "Runge-Kutta (6th order, Butcher)"); } }
	public static class VernerRK8Solver 	extends RungeKuttaSolver { public VernerRK8Solver(Physics2D physics) 	{ super(physics, ButcherTable.RK8_VERNER, "Runge-Kutta (8th order, Verner)"); } }
}
