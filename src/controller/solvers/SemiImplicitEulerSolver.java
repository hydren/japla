package controller.solvers;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.physics.Body2D;

public class SemiImplicitEulerSolver extends Physics2DSolver
{
	public 
	SemiImplicitEulerSolver(Physics2D physics) 
	{
		super(physics);
		timestep = 0.1;
		displayName = "Semi-implicit Euler";
	}
	
	@Override
	public Physics2DSolver getNewInstance(Physics2D physics) {
		return new SemiImplicitEulerSolver(physics);
	}
	
	@Override
	public void step()
	{
		//position loop
		for(Body2D body : physics.universe.bodies)
		{
			body.position.add(body.velocity.times(timestep));
		}
		
		physics.computeAccelerations();
		
		//velocity loop
		for(Body2D body : physics.universe.bodies)
		{
			body.velocity.add(body.acceleration.times(timestep));
		}
		
		timeElapsed += timestep;
	}
}
