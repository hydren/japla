package controller.solvers;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.physics.Body2D;

public class EulerSolver extends Physics2DSolver
{
	public 	EulerSolver(Physics2D physics) 
	{
		super(physics);
		timestep = 0.01;
		displayName = "Euler";
	}
	
	@Override
	public Physics2DSolver getNewInstance(Physics2D physics) {
		return new EulerSolver(physics);
	}

	@Override public 
	void step() 
	{
		physics.computeAccelerations();
		
		//velocity & position loop
		for(Body2D b1 : physics.universe.bodies)
		{
			b1.position.add(b1.velocity.times(timestep));
			b1.velocity.add(b1.acceleration.times(timestep));
		}
		
		timeElapsed += timestep;
	}
}
