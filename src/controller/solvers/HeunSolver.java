package controller.solvers;

import java.util.HashMap;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.geometry.Vector2D;
import model.physics.Body2D;

public class HeunSolver extends Physics2DSolver 
{
	public 
	HeunSolver(Physics2D physics) 
	{
		super(physics);
		timestep = 0.1;
		displayName = "Improved Euler/Heun (RK2)";
	}
	
	@Override 
	public 	void step() 
	{
		HashMap<Body2D, Vector2D> 
			estimatedPosition = new HashMap<Body2D, Vector2D>(), 
			estimatedVelocity = new HashMap<Body2D, Vector2D>(), 
			estimatedAcceleration = new HashMap<Body2D, Vector2D>();
		
		physics.computeAccelerations();

		for(Body2D body : physics.universe.bodies)
		{
			estimatedPosition.put(body, body.position.sum(body.velocity.times(timestep)));
			estimatedVelocity.put(body, body.velocity.sum(body.acceleration.times(timestep)));
		}

		physics.computeAccelerations(estimatedAcceleration, estimatedPosition);

		//velocity & position loop
		for(Body2D body : physics.universe.bodies)
		{
			body.position.add((body.velocity.sum(estimatedVelocity.get(body))).scale(timestep * 0.5));
			body.velocity.add((body.acceleration.sum(estimatedAcceleration.get(body))).scale(timestep * 0.5));
		}

		timeElapsed += timestep;
	}

	@Override 
	public Physics2DSolver getNewInstance(Physics2D p) 
	{
		return new HeunSolver(p);
	}

}
