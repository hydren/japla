package controller.solvers;

import controller.Physics2D;
import controller.Physics2DSolver;
import model.physics.Body2D;

public class LeapfrogSolver extends Physics2DSolver
{
	public 
	LeapfrogSolver(Physics2D physics) 
	{
		super(physics);
		timestep = 0.1;
		displayName = "Leapfrog";
	}
	
	@Override
	public Physics2DSolver getNewInstance(Physics2D physics) {
		return new LeapfrogSolver(physics);
	}

	@Override
	public void step() {
		physics.computeAccelerations();

		for(Body2D body : physics.universe.bodies)
		{
			if(timeElapsed == 0)
				body.velocity.add(body.acceleration.times(timestep*0.5));
			else
				body.velocity.add(body.acceleration.times(timestep));
			
			body.position.add(body.velocity.times(timestep));
		}
		
		timeElapsed += timestep;
	}

}
