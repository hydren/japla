package controller;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import controller.solvers.AvailableSolvers;
import model.geometry.Vector2D;
import model.physics.Body2D;
import model.physics.Universe2D;

public class Physics2D 
{
	/** The universe instance: all bodies and gravity. 
	 * <b>This instance shouldn't be exchanged directly.</b> 
	 * Use {@link #setUniverse(Universe2D) setUniverse} instead. */
	public Universe2D universe = new Universe2D();
	
	public Physics2D() 
	{
		Physics2DSolver solver = AvailableSolvers.getSolverByClassName(Main.settings.solver);
		if(solver == null) solver = AvailableSolvers.getSolverByClassName("default");
		this.setSolver(solver.getNewInstance(this));
	}
	
	public class ReferenceFrame
	{
		private Vector2D customPosition = null, customVelocity = null;
		private List<Body2D> bodies = new ArrayList<Body2D>();
		
		public void set(Vector2D customPos, Vector2D customVel)
		{
			this.customPosition = customPos;
			this.customVelocity = customVel;
			bodies = null;
		}
		
		public void set(Body2D[] reference)
		{
			this.customPosition = this.customVelocity = null;
			
			if(this.bodies == null) 
				this.bodies = new ArrayList<Body2D>();
			else 
				this.bodies.clear();
			
			for(Body2D b : reference) this.bodies.add(b);
		}
		
		public void set(List<Body2D> reference)
		{
			this.customPosition = this.customVelocity = null;
			
			if(this.bodies == null) 
				this.bodies = new ArrayList<Body2D>(reference);
			
			else
			{
				this.bodies.clear();
				this.bodies.addAll(reference);
			}
		}
		
		public boolean dissociate(Body2D body)
		{
			if(this.bodies != null && this.bodies.contains(body))
			{
				this.bodies.remove(body);
				return true;
			}
			else return false;
		}
		
		public void reset()
		{
			bodies = null;
			customPosition = new Vector2D();
			customVelocity = new Vector2D();
		}
		
		public Vector2D getPosition()
		{
			if(bodies == null)
				return customPosition;
			else if(bodies.size() == 1)
				return bodies.get(0).position.clone();
			else
			{
				Vector2D centerOfMass = new Vector2D();
				double totalMass = 0;
				for(Body2D b : bodies)
				{
					centerOfMass.add(b.position.times(b.mass));
					totalMass += b.mass;
				}
				
				if(totalMass != 0)
					centerOfMass.scale(1.0d/totalMass);
					
				return centerOfMass;
			}
		}
		
		public Vector2D getVelocity()
		{
			if(bodies == null)
				return customVelocity;
			
			else if(bodies.size() == 1)
				return bodies.get(0).velocity.clone();
			
			else
			{
				Vector2D totalVelocity = new Vector2D();
				double totalMass = 0;
				for(Body2D b : bodies)
				{
					totalVelocity.add(b.velocity.times(b.mass));
					totalMass += b.mass;
				}
				
				if(totalMass != 0)
					totalVelocity.scale(1.0d/totalMass);
				
				return totalVelocity;
			}
		}
		
		public double getTotalMass()
		{
			if(bodies == null)
				return 0;
			
			else if(bodies.size() == 0)
				return bodies.get(0).mass;
			
			else
			{
				double totalMass = 0;
				for(Body2D b : bodies)
					totalMass += b.mass;
				
				return totalMass;
			}
		}
		
		public boolean isPointLike()
		{
			return (bodies == null);
		}
	}
	public ReferenceFrame referenceFrame = new ReferenceFrame();
	
	
	/** The current physics simulation class. 
	 * <b>This instance shouldn't be exchanged directly.</b> 
	 * Use {@link #setSolver(Physics2DSolver) setSolver} instead. */
	public Physics2DSolver solver;
	
	@Override
	public Physics2D clone()
	{
		Physics2D p = new Physics2D();
		synchronized(this)
		{
			p.solver.timestep = solver.timestep;
			p.universe.gravity = universe.gravity;
			for(Body2D b : universe.bodies)
			{
				p.universe.bodies.add(b);
			}
		}
		return p;
	}
	
	private ArrayList<ArrayList<Body2D>> collisions = new ArrayList<ArrayList<Body2D>>();
	private void resolveCollisions()
	{
		//detect collisions
		collisions.clear();
		for(Body2D a : universe.bodies)	for(Body2D b : universe.bodies)
		{
			if(a == b) continue;
				
			if(a.position.distance(b.position) < a.diameter/2 + b.diameter/2)
			{
				boolean bothAdded=false;
				for(ArrayList<Body2D> set : collisions)
				{
					if(set.contains(a) && set.contains(b)) //probably a duplicate lookup
					{
						bothAdded = true;
						break;
					}
					else if(set.contains(a) && !set.contains(b)) //append colliding b
					{
						set.add(b);
						bothAdded = true;
						break;
					}
					else if(!set.contains(a) && set.contains(b)) //append colliding a
					{
						set.add(a);
						bothAdded = true;
						break;
					}
				}
				if(!bothAdded) //new colliding pair
				{
					ArrayList<Body2D> newSet = new ArrayList<Body2D>();
					newSet.add(a);
					newSet.add(b);
					collisions.add(newSet);
				}
			}
		}
		
		if(collisions.isEmpty() == false)
			referenceFrame.bodies.clear();
		
		//resolve collisions
		for(ArrayList<Body2D> collisionSet : collisions)
		{
			Body2D merger = new Body2D(0, 0, new Vector2D(), new Vector2D(), new Vector2D());
			for(Body2D body : collisionSet)
			{
				merger.position.x += body.position.x;
				merger.position.y += body.position.y;

				merger.velocity.x = (merger.velocity.x*merger.mass + body.velocity.x*body.mass)/(merger.mass + body.mass);
				merger.velocity.y = (merger.velocity.y*merger.mass + body.velocity.y*body.mass)/(merger.mass + body.mass);
				
				merger.diameter = Math.sqrt(merger.diameter*merger.diameter + body.diameter*body.diameter);
				merger.mass += body.mass;

				merger.color = new Color((merger.color.getRed() + body.color.getRed())/2, (merger.color.getGreen() + body.color.getGreen())/2, (merger.color.getBlue() + body.color.getBlue())/2);
				
				universe.bodies.remove(body);
			}
			
			if(collisionSet.size() == 0) continue;
			
			merger.position.x /= collisionSet.size();
			merger.position.y /= collisionSet.size();
			
			universe.bodies.add(merger);

			//notify listeners about the collision
			for(BodyCollisionListener listener : registeredBodyCollisionListeners)
				listener.onBodyCollision(collisionSet, merger);
		}
	}
	
	/** Computes and updates all bodies' accelerations based on their mutual gravitation forces, using their current positions. */
	public void computeAccelerations()
	{
		for(Body2D b1 : universe.bodies)
		{
			if(b1.acceleration == null) b1.acceleration = new Vector2D();
			b1.acceleration.scale(0);
			
			for(Body2D b2 : universe.bodies) if(b1 != b2)
			{
				double magnitude = (-universe.gravity * b1.mass * b2.mass) / Math.pow(b1.position.distance(b2.position), 2.0);
				b1.acceleration.add(b1.position.difference(b2.position).normalize().scale(magnitude/b1.mass));
			}
		}
	}
	
	/** Computes the acceleration (resulting from mutual gravitation forces) of all bodies, using their current positions.
	 *  The resulting accelerations are stored on 'resultingAccelerations'. */
	public void computeAccelerations(Map<Body2D, Vector2D> resultingAccelerations)
	{
		for(Body2D b1 : universe.bodies)
		{
			resultingAccelerations.put(b1, new Vector2D());
			for(Body2D b2 : universe.bodies) if(b1 != b2)
			{
				double magnitude = (-universe.gravity * b1.mass * b2.mass) / Math.pow(b1.position.distance(b2.position), 2.0);
				resultingAccelerations.get(b1).add(b1.position.difference(b2.position).normalize().scale(magnitude/b1.mass));
			}
		}
	}
	
	/** Computes the acceleration (resulting from mutual gravitation forces) of all bodies, at the specified positions (instead of the bodies' current position).
	 *  The resulting accelerations are stored on 'resultingAccelerations'. */
	public void computeAccelerations(Map<Body2D, Vector2D> resultingAccelerations, Map<Body2D, Vector2D> positions)
	{
		for(Body2D b1 : universe.bodies)
		{
			resultingAccelerations.put(b1, new Vector2D());
			for(Body2D b2 : universe.bodies) if(b1 != b2)
			{
				double magnitude = (-universe.gravity * b1.mass * b2.mass) / Math.pow(positions.get(b1).distance(positions.get(b2)), 2.0);
				resultingAccelerations.get(b1).add(positions.get(b1).difference(positions.get(b2)).normalize().scale(magnitude/b1.mass));
			}
		}
	}
	
	public void derive(Map<Body2D, Vector2D> dvdt, Map<Body2D, Vector2D> dydt)
	{
		computeAccelerations(dvdt);
		
		for(Body2D body : universe.bodies)
			dydt.put(body, body.velocity.clone());
	}
	
	public void derive(Map<Body2D, Vector2D> dvdt, Map<Body2D, Vector2D> dydt, Map<Body2D, Vector2D> vn, Map<Body2D, Vector2D> yn)
	{
		computeAccelerations(dvdt, yn);
		dydt.clear();
		for(Body2D body : vn.keySet())
			dydt.put(body, vn.get(body).clone());
	}
	
	public void step()
	{
		//update positions
		solver.step();
		
		//resolve collisions
		resolveCollisions();
	}
	
	
	public void changeReferenceFrameTo(List<Body2D> reference)
	{
		//compute center of mass of 'reference'
		//compute total momentum for 'reference'
		//subtract from all bodies the position and speed
	}
	
	/** Safe way to change the universe instance. */
	public void setUniverse(Universe2D u)
	{
		universe = u;
	}
	
	/** Safe way to change solver instance. The solver's Universe2D instance will be replaced by this instance's Univers2D */
	public void setSolver(Physics2DSolver s)
	{
		if(solver != null)
		{
			s.timeElapsed = solver.timeElapsed;
			s.timestep = solver.timestep;
		}
		s.physics = this;
		solver = s;
	}
	
	
	//collision listeners and class
	public static interface BodyCollisionListener {
		public void onBodyCollision(List<Body2D> collidingSet, Body2D resultingMerger);
	}
	
	private Set<BodyCollisionListener> registeredBodyCollisionListeners = new HashSet<BodyCollisionListener>();
	
	public void addBodyCollisionListener(BodyCollisionListener listener) {
		registeredBodyCollisionListeners.add(listener);
	}
	
	public void removeBodyCollisionListener(BodyCollisionListener listener){
		registeredBodyCollisionListeners.remove(listener);
	}
}
