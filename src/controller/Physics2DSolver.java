package controller;

public abstract 
class Physics2DSolver 
{
	public 	String displayName;
	public 	double timestep;
	public	double timeElapsed=0;
	public	Physics2D physics;
	
	protected Physics2DSolver() {}
	
	protected Physics2DSolver(Physics2D physics)
	{
		this.physics = physics;
	}
	
	public abstract void step();
	
	/** Helper method to create an instance of this solver. */
	public abstract Physics2DSolver getNewInstance(Physics2D physics);
	
	@Override 
	public String toString()
	{
		return displayName;
	}
}
